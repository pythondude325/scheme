; Horrors from beyond

; idea:
; msprintf result_str, "%s:%d: %s", __?FILE?__, __?LINE?__, "failed assertion"
%macro msprintf 2-*
    %defalias %%destination %[%1]
    %strcat %%format_string %2
    %strlen %%format_string_length %%format_string
    %strcat %%output_string ""

    %rotate 2

    %assign %%current_position 1

    %strcat %%temp ""

    %rep %%format_string_length
        %if %%current_position > %%format_string_length
            %exitrep
        %endif

        %substr %%current_char %%format_string %%current_position 

        %if %%current_char = '%'
            %assign %%current_position (%%current_position + 1)
            %substr %%format_spec %%format_string %%current_position

            %if %%format_spec = 'd'
                stringify %%temp, %1
                %rotate 1
            %elif %%format_spec = 's'
                %strcat %%temp %1
                %rotate 1
            %elif %%format_spec = '%'
                %strcat %%temp '%'
            %else
                %error "Invalid format specifier"
            %endif
            %strcat %%output_string %%output_string %%temp
        %else
            %strcat %%output_string %%output_string %%current_char
        %endif

        %assign %%current_position (%%current_position + 1)
    %endrep

    %xdefine %%destination %%output_string
%endmacro



%macro stringify 2
    %strcat %%output ""
    %assign %%i %2
    %if %%i < 0
        %assign %%i (-%%i)
    %endif
    %rep 100
        %assign %%d (%%i % 10)
        %assign %%i (%%i / 10)

        %if %%d = 0
            %strcat %%output "0" %%output 
        %elif %%d = 1
            %strcat %%output "1" %%output 
        %elif %%d = 2
            %strcat %%output "2" %%output
        %elif %%d = 3
            %strcat %%output "3" %%output
        %elif %%d = 4
            %strcat %%output "4" %%output
        %elif %%d = 5
            %strcat %%output "5" %%output
        %elif %%d = 6
            %strcat %%output "6" %%output
        %elif %%d = 7
            %strcat %%output "7" %%output
        %elif %%d = 8
            %strcat %%output "8" %%output
        %elif %%d = 9
            %strcat %%output "9" %%output
        %else
            %error "Integer arithmetic error"
        %endif

        %if %%i = 0
            %exitrep
        %endif
    %endrep

    %if %2 < 0
        %strcat %%output "-" %%output
    %endif

    %strcat %1 %%output
%endmacro