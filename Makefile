NASM_FLAGS = -DDEBUG

all: scheme

clean:
	rm scheme scheme.o

scheme: src/*.asm src/**/*.asm
	nasm -f elf -F dwarf -g src/scheme.asm -o scheme.o -Isrc/ $(NASM_FLAGS)
	gcc -no-pie -g -m32 -o scheme scheme.o -ljemalloc

