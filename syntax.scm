(syntax-rules ()
    ((or) #f)
    ((or e) e)
    ((or e es ...)
        (let ((temp e))
            (if temp
                temp
                (or es ...)))))

                