%ifndef _HASHTABLE_ASM
%define _HASHTABLE_ASM

%include "common.asm"
%include "slice.asm"
%include "hash.asm"
%use ifunc

; load factor will be 3/4

struc hashtable
    .bucket_slice: resb slice_size
    .count: resd 1
    .capacity: resd 1
    .key_type_vtab: resd 1

    ; view offsets for easy access
    .buckets equ .bucket_slice + slice.ptr
    ; keep a separate variable for capacity anyway
    ; .capacity equ .bucket_slice + slice.len

    .empty_marker equ 0
    .tombstone_marker equ 1
endstruc

struc table_entry
    .key: resd 1
    .data: resd 1
endstruc

segment .text

extern memset

; Creat a new hashtable at `dest`
; 
; Default hashtable size is 32 buckets
; 
; Arguments:
; - dest: pointer to hashtable struct
; - key_type_vtab: pointer to the vtable for the key type you want to use.
;       should probably be `hashtable_key_int_vtab` or `hashtable_key_cstr_vtab`
hashtable_new:
    %push
    marg .dest:dword, .key_type_vtab:dword
    menter

    mov edi, dword [ebp + .dest]

    push 32 * table_entry_size
    pushlea eax, dword [edi + hashtable.bucket_slice]
    call slice_new
    popn 2

    mov edi, dword [ebp + .dest]
    mov dword [edi + hashtable.capacity], 32
    mov dword [edi + hashtable.count], 0
    mov eax, dword [ebp + .key_type_vtab]
    mov dword [edi + hashtable.key_type_vtab], eax

    leave
    ret
    %pop

hashtable_delete:
    %push
    marg .dest:dword
    menter

    mov edi, dword [ebp + .dest]

    pushlea eax, [edi + hashtable.bucket_slice]
    call slice_delete
    popn 1

    leave
    ret
    %pop

hashtable_capacity_check:
    %push
    marg .table:dword
    menter

    mov edi, dword [ebp + .table]
    tzcnt ecx, dword [edi + hashtable.capacity]
    sub cl, 2
    mov eax, dword [edi + hashtable.count]
    shr eax, cl

    leave
    ret
    %pop

; new capacity must be a multiple of two and must be enough for the number of
; current elements
hashtable_rehash:
    %push
    marg .table:dword, .new_capacity:dword
    mlocal .old_buckets:slice_size, .old_capacity:dword
    menter

    mov edi, dword [ebp + .table]

    ; sanity check
    mov eax, dword [edi + hashtable.count]
    cmp eax, dword [ebp + .new_capacity]
    assert b, "Bad hashtable rehash"

    ; move old slice
    movq xmm0, qword [edi + hashtable.bucket_slice]
    movq qword [ebp + .old_buckets], xmm0

    ; allocate new slice
    mov eax, dword [ebp + .new_capacity]
    shl eax, ilog2(table_entry_size)
    push eax ; size: new_capacity * table_entry_size
    pushlea eax, dword [edi + hashtable.bucket_slice]
    call slice_new
    popn 2

    mov edi, dword [ebp + .table]

    mov ebx, dword [edi + hashtable.capacity]
    mov dword [ebp + .old_capacity], ebx

    mov ebx, dword [ebp + .new_capacity]
    mov dword [edi + hashtable.capacity], ebx ; set new capacity
    mov dword [edi + hashtable.count], 0 ; reset length

    mov edx, 0 ; edx holds old bucket index
    .bucket_loop:
        mov esi, dword [ebp + .old_buckets + slice.ptr]

        mov eax, dword [esi + edx*table_entry_size + table_entry.key]
        cmp eax, hashtable.empty_marker
        je .bucket_loop_cont
        cmp eax, hashtable.tombstone_marker
        je .bucket_loop_cont

        ; TODO: Should probaby inline this function call

        push edx

        push eax
        push dword [ebp + .table]
        call hashtable_insert_assume_capacity
        popn 2

        pop edx

        ; copy data from old table to new
        mov esi, dword [ebp + .old_buckets + slice.ptr]
        mov ebx, dword [esi + edx*table_entry_size + table_entry.data]
        mov dword [eax + table_entry.data], ebx
    .bucket_loop_cont:
    inc edx
    cmp edx, dword [ebp + .old_capacity]
    jb .bucket_loop

    pushlea eax, [ebp + .old_buckets]
    call slice_delete
    popn 1

    leave
    ret
    %pop

hashtable_insert:
    %push
    marg .table:dword, .key:dword
    menter

    push dword [ebp + .table]
    call hashtable_capacity_check
    popn 1

    cmp eax, 3
    jb .good_capacity
        mov edi, dword [ebp + .table]
        mov eax, dword [edi + hashtable.capacity]
        shl eax, 1 ; double capacity
        push eax
        push dword [ebp + .table]
        call hashtable_rehash
        popn 2
    .good_capacity:

    ; Tail call into hashtable_insert_assume_capacity
    leave
    jmp hashtable_insert_assume_capacity
    %pop

hashtable_insert_assume_capacity:
    %push
    marg .table:dword, .key:dword
    mlocal .bucket_index:dword, .capacity_mask:dword
    menter

    push dword [ebp + .key]
    push dword [ebp + .table]
    call hashtable_hash_key
    popn 2

    mov edi, dword [ebp + .table]
    mov ebx, dword [edi + hashtable.capacity]
    dec ebx
    mov dword [ebp + .capacity_mask], ebx

    and eax, dword [ebp + .capacity_mask]
    mov dword [ebp + .bucket_index], eax

    jmp .probing_loop_cond
    .probing_loop:
        mov eax, dword [ebp + .bucket_index]
        inc eax
        and eax, dword [ebp + .capacity_mask]
        mov dword [ebp + .bucket_index], eax
    .probing_loop_cond:
    mov edi, dword [ebp + .table]
    mov esi, dword [edi + hashtable.buckets]
    mov eax, dword [ebp + .bucket_index]

    mov ecx, dword [esi + eax*table_entry_size + table_entry.key]
    cmp ecx, hashtable.empty_marker
    je .found_slot
    cmp ecx, hashtable.tombstone_marker
    je .found_slot

    push ecx
    push dword [ebp + .key]
    push dword [ebp + .table]
    call hashtable_equal_key
    popn 3
    cmp eax, 1
    jne .probing_loop

    ; found matching entry
    mov edi, dword [ebp + .table]
    mov esi, dword [edi + hashtable.buckets]
    mov eax, dword [ebp + .bucket_index]
    lea eax, dword [esi + eax*table_entry_size]
    leave
    ret

    ; found slot
    .found_slot:
    mov edi, dword [ebp + .table]
    mov esi, dword [edi + hashtable.buckets]
    mov eax, dword [ebp + .bucket_index]
    mov edx, dword [ebp + .key]
    mov dword [esi + eax*table_entry_size + table_entry.key], edx
    inc dword [edi + hashtable.count]

    lea eax, dword [esi + eax*table_entry_size]
    leave
    ret

    %pop

hashtable_remove:
    %push
    marg .table:dword, .key:dword
    menter

    push dword [ebp + .table]
    call hashtable_capacity_check
    popn 1

    mov edi, dword [ebp + .table]

    cmp eax, 0
    ja .good_capacity
        mov edi, dword [ebp + .table]
        mov eax, dword [edi + hashtable.capacity]

        ; don't actually shrink the table if we are already at 32 elements
        cmp eax, 32
        jbe .good_capacity

        shr eax, 1 ; half of previous capacity
        push eax
        push dword [ebp + .table]
        call hashtable_rehash
        popn 2
    .good_capacity:

    ; tail call into hashtable_remove_assume_capacity
    leave
    jmp hashtable_remove_assume_capacity
    %pop

hashtable_remove_assume_capacity:
    %push
    marg .table:dword, .key:dword
    menter

    push dword [ebp + .key]
    push dword [ebp + .table]
    call hashtable_get
    popn 2

    cmp eax, 0
    jz .not_found ; the key was not found in the table

    push eax
    push dword [ebp + .table]
    call hashtable_remove_entry
    popn 2

    .not_found:
    leave
    ret
    %pop

; Remove an entry from the table
; 
; Arguments:
; - table: pointer to table
; - entry: pointer to entry in table
hashtable_remove_entry:
    %push
    marg .table:dword, .entry:dword
    menter

    ; replace the entry key with a tombstone
    mov eax, dword [ebp + .entry]
    mov dword [eax + table_entry.key], hashtable.tombstone_marker

    ; decrement the count on the table
    mov eax, dword [ebp + .table]
    dec dword [eax + hashtable.count]

    mov eax, 0
    leave
    ret
    %pop

hashtable_get:
    %push
    marg .table:dword, .key:dword
    mlocal .bucket_index:dword, .capacity_mask:dword
    menter

    ; registers:
    ; - eax: bucket index
    ; - ebx: capacity mask
    ; - ecx: current entry key
    ; - esi: buckets pointer
    ; - edi: table pointer

    push dword [ebp + .key]
    push dword [ebp + .table]
    call hashtable_hash_key
    popn 2
    ; eax holds hash of key

    mov edi, dword [ebp + .table] ; edi holds pointer to table

    mov ebx, dword [edi + hashtable.capacity]
    dec ebx ; ebx holds capacity mask
    mov dword [ebp + .capacity_mask], ebx

    and eax, ebx ; mask off the high bits with the capacity mask to get bucket index
    mov dword [ebp + .bucket_index], eax

    mov esi, dword [edi + hashtable.buckets]
    jmp .probing_loop_cond
    .probing_loop:
        mov eax, dword [ebp + .bucket_index]
        inc eax ; increment the bucket index
        and eax, dword [ebp + .capacity_mask] ; loop it around if it overflows
        mov dword [ebp + .bucket_index], eax
    .probing_loop_cond:
    mov edi, dword [ebp + .table]
    mov esi, dword [edi + hashtable.buckets]
    mov eax, dword [ebp + .bucket_index]
    mov ecx, dword [esi + eax*table_entry_size + table_entry.key] ; ecx holds current entry key

    cmp ecx, hashtable.empty_marker
    je .not_found
    ; do nothing for tombstones

    push ecx
    push dword [ebp + .key]
    push dword [ebp + .table]
    call hashtable_equal_key
    popn 3
    cmp eax, 1
    jne .probing_loop

    ; found entry
    mov edi, dword [ebp + .table]
    mov esi, dword [edi + hashtable.buckets]
    mov eax, dword [ebp + .bucket_index]
    lea eax, [esi + eax*table_entry_size]
    leave
    ret

    ; not found entry
    .not_found:
    mov eax, 0
    leave
    ret
    %pop

; VTables for different types of keys in the hashtable

hashtable_hash_key:
    %push
    marg .table:dword, .key:dword
    menter

    mov edi, dword [ebp + .table]
    mov eax, dword [edi + hashtable.key_type_vtab]
    mov eax, dword [eax + hashtable_key_type_vtab.hash]

    push dword [ebp + .key]
    call eax
    popn 1

    leave
    ret
    %pop

hashtable_equal_key:
    %push
    marg .table:dword, .a:dword, .b:dword
    menter

    mov edi, dword [ebp + .table]
    mov eax, dword [edi + hashtable.key_type_vtab]
    mov eax, dword [eax + hashtable_key_type_vtab.equal]

    push dword [ebp + .b]
    push dword [ebp + .a]
    call eax
    popn 1

    leave
    ret
    %pop

struc hashtable_key_type_vtab
    .hash: resd 1 ; dword -> dword
    .equal: resd 1 ; (dword, dword) -> 1 or 0
endstruc

segment .rodata
    hashtable_key_int_vtab: dd hash_int, equal_int
    hashtable_key_cstr_vtab: dd hash_cstr, equal_cstr
    hashtable_key_slice_vtab: dd hash_slice, equal_slice

segment .text

hash_int:
    %push
    marg .src:dword
    menter

    push 0
    push dword [ebp + .src]
    call xxh32_dword
    popn 2

    leave
    ret
    %pop

equal_int:
    %push
    marg .a:dword, .b:dword
    menter

    mov ebx, dword [ebp + .a]
    cmp ebx, dword [ebp + .b]
    je .equal

    mov eax, 0
    leave
    ret

    .equal:
    mov eax, 1
    leave
    ret
    %pop

extern strlen 

hash_cstr:
    %push
    marg .src:dword
    menter

    push dword [ebp + .src]
    call strlen
    popn 1

    push 0
    push eax
    push dword [ebp + .src]
    call xxh32
    popn 3
    
    leave
    ret
    %pop

extern strcmp

equal_cstr:
    %push
    marg .a:dword, .b:dword
    menter

    push dword [ebp + .a]
    push dword [ebp + .b]
    call strcmp
    popn 2

    cmp eax, 0
    je .same

    mov eax, 0
    leave
    ret

    .same:
    mov eax, 1
    leave
    ret

hash_slice:
    %push
    marg .src:dword
    menter

    push 0
    push dword [ebp + .src]
    call xxh32_slice
    popn 2

    leave
    ret
    %pop

extern memcmp

equal_slice:
    %push
    marg .a:dword, .b:dword
    menter

    mov eax, dword [ebp + .a]
    mov ebx, dword [ebp + .b]

    ; mprintfn "a:%08x b:%08x", eax, ebx
    ; mprintfn "%08x %08x", dword [eax + slice.len], dword [ebx + slice.len]

    mov ecx, dword [eax + slice.len]
    cmp ecx, dword [ebx + slice.len]
    jne .fail

    mov eax, dword [eax + slice.ptr]
    mov ebx, dword [ebx + slice.ptr]

    ; mprintfn "comparing slices '%s' and '%s'", eax, ebx

    push ecx
    push ebx
    push eax
    call memcmp
    popn 3

    cmp eax, 0
    jne .fail
    mov eax, 1
    leave
    ret

    .fail:
    mov eax, 0
    leave
    ret
    %pop

%endif