%ifndef _OBJECTS_NULL_ASM
%define _OBJECTS_NULL_ASM

segment .text

null_new:
    %push
    marg .dest:dword
    menter

    mov eax, dword [ebp + .dest]
    mov word [eax + obj.tag], object_tag.null
    mov dword [eax + obj.extra], 0

    leave
    ret
    %pop

obj_get_refs_none null
obj_get_allocs_none null
null_is_terminal equ true

register_object null

%endif