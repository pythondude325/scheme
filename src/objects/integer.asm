%ifndef _OBJECTS_INTEGER_ASM
%define _OBJECTS_INTEGER_ASM

segment .text

integer_new:
    %push
    marg .dest:dword, .value:dword
    menter

    mov eax, dword [ebp + .dest]
    mov word [eax + obj.tag], object_tag.integer
    mov ebx, dword [ebp + .value]
    mov dword [eax + obj.extra], ebx

    leave
    ret
    %pop

obj_get_refs_none integer
obj_get_allocs_none integer
integer_is_terminal equ true

register_object integer

%endif