%ifndef _OBJECTS_SYMBOL_ASM
%define _OBJECTS_SYMBOL_ASM

%include "interner.asm"

segment .text

symbol_new:
    %push
    marg .dest:dword, .string:dword
    menter

    mov ebx, dword [ebp + .dest]
    mov word [ebx + obj.tag], object_tag.symbol

    push dword [ebp + .string]
    call interner_get_or_intern_cstr
    popn 1

    mov ebx, dword [ebp + .dest]
    mov dword [ebx + obj.extra], eax

    leave
    ret
    %pop

symbol_new_from_slice:
    %push
    marg .dest:dword, .slice_p:dword
    menter
    
    mov ebx, dword [ebp + .dest]
    mov word [ebx + obj.tag], object_tag.symbol

    push dword [ebp + .slice_p]
    call interner_get_or_intern_slice
    popn 1

    mov ebx, dword [ebp + .dest]
    mov dword [ebx + obj.extra], eax

    leave
    ret
    %pop
    
symbol_get_string:
    %push
    marg .src:dword
    menter

    mov eax, dword [ebp + .src]
    cmp dword [eax + obj.tag], object_tag.symbol
    assert e, "Incorrect object type"

    mov eax, [eax + obj.extra]
    push eax
    call interner_resolve
    popn 1

    mov eax, dword [eax + slice.ptr]

    leave
    ret
    %pop

obj_get_refs_none symbol
obj_get_allocs_none symbol
symbol_is_terminal equ true

register_object symbol

%endif