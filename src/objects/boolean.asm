%ifndef _OBJECTS_BOOLEAN_ASM
%define _OBJECTS_BOOLEAN_ASM

segment .text

; Booleans
;
; 1 for true
; 0 for false

boolean_new:
    %push
    marg .dest:dword, .value:dword
    menter

    mov eax, dword [ebp + .dest]
    mov word [eax + obj.tag], object_tag.boolean
    mov ebx, dword [ebp + .value]
    mov dword [eax + obj.extra], ebx

    leave
    ret
    %pop

boolean_true_new:
    %push
    marg .dest:dword
    menter

    push 1
    push dword [ebp + .dest]
    call boolean_new
    popn 1

    leave
    ret
    %pop

boolean_false_new:
    %push
    marg .dest:dword
    menter

    push 0
    push dword [ebp + .dest]
    call boolean_new
    popn 1
    
    leave
    ret
    %pop

; Test a value for truthyness
;
; #f is falsy, all other values are truthy
boolean_test:
    %push
    marg .src:dword
    menter

    mov eax, [ebp + .src]
    cmp word [eax + obj.tag], object_tag.boolean
    jne .success
    cmp dword [eax + obj.extra], 0
    jne .success

    mov eax, 0
    leave
    ret

    .success:
    mov eax, 1
    leave
    ret
    %pop

obj_get_refs_none boolean
obj_get_allocs_none boolean
boolean_is_terminal equ true

register_object boolean

%endif