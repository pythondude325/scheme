%ifndef _OBJECTS_PAIR_ASM
%define _OBJECTS_PAIR_ASM

struc pair_alloc
    .car: resb obj_size
    .cdr: resb obj_size
endstruc

; object model functions
pair_get_refs_count:
    mov eax, 2
    ret

pair_get_refs:
    %push
    marg .dest:dword, .src:dword
    menter

    ; TODO: make sure this works, i think it might be a lil broken

    mov ecx, dword [ebp + .dest]

    mov eax, dword [ebp + .src]
    mov eax, dword [eax + obj.extra]
    lea ebx, dword [eax + pair_alloc.car]
    mov dword [ecx + 4*0], ebx
    lea ebx, dword [eax + pair_alloc.cdr]
    mov dword [ecx + 4*1], ebx

    leave
    ret
    %pop

pair_get_allocs_count:
    mov eax, 1
    ret

pair_get_allocs:
    %push
    marg .dest:dword, .src:dword
    menter

    mov eax, dword [ebp + .src]
    mov eax, dword [eax + obj.extra]
    mov ebx, dword [ebp + .dest]
    mov dword [ebx], eax

    leave
    ret
    %pop



; arguments:
; - dest: pointer to write pair at
; returns: nothing
pair_new:
    %push
    marg .dest:dword
    menter

    mov edi, dword [ebp + .dest]

    mov word [edi + obj.tag], object_tag.pair

    push dword pair_alloc_size
    call gc_allocate
    mov edi, dword [ebp + .dest]
    mov dword [edi + obj.extra], eax

    leave
    ret
    %pop

; arguments:
; - src: pointer to pair object
; returns: pointer to car object in pair
pair_car:
    %push
    marg .src:dword
    menter

    mov eax, dword [ebp + .src]
    mov eax, dword [eax + obj.extra]
    lea eax, dword [eax + pair_alloc.car]

    leave
    ret
    %pop

; arguments:
; - src: pointer to pair object
; returns: pointer to cdr object in pair
pair_cdr:
    %push
    marg .src:dword
    menter

    mov eax, dword [ebp + .src]
    mov eax, dword [eax + obj.extra]
    lea eax, dword [eax + pair_alloc.cdr]
    
    leave
    ret
    %pop

pair_is_terminal equ false
register_object pair

%endif