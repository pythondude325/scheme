%ifndef _PARSE_NUMBER_ASM
%define _PARSE_NUMBER_ASM

struc ureal_integer
    .digits: resb slice_size
endstruc

struc ureal_rational
    .num_digits: resb slice_size
    .den_digits: resb slice_size
endstruc

struc ureal_decimal
    .whole_digits: resb slice_size
    .fract_digits: resb slice_size
    .expt: resd 1
endstruc

tagged_union ureal, ureal_integer, ureal_rational, ureal_decimal

struc real
    .sign: resd 1
    .ureal: resb ureal_size
endstruc

segment .rodata
    digit_radix_table:
        times 16 db -1 ; 0x00
        times 16 db -1 ; 0x10
        times 16 db -1 ; 0x20
        db  2, 2, 8, 8,  8, 8, 8, 8, 10,10,-1,-1, -1,-1,-1,-1 ; 0x30
        db -1,16,16,16, 16,16,16,-1, -1,-1,-1,-1, -1,-1,-1,-1 ; 0x40
        times 16 db -1 ; 0x50
        db -1,16,16,16, 16,16,16,-1, -1,-1,-1,-1, -1,-1,-1,-1 ; 0x60
        times 16 db -1 ; 0x70
        times 128 db -1 ; Non ascii chars

    number_input_format: db "%d",0

segment .text

parse_number_digit_to_value:
    %push
    marg .digit:dword
    menter

    mov eax, dword [ebp + .digit]
    cmp eax, 'A'
    jnae .decimal
    
    or eax, 0b1000000
    add eax, 10 - 'a'
    leave
    ret

    .decimal:
    sub eax, '0'
    leave
    ret
    %pop

extern sscanf

parse_number:
    %push
    marg .token_ptr:dword, .error_ptr:dword, .dest:dword
    mlocal .radix:dword, .exactness:dword, \
        .working_slice:slice_size, .real:real_size
    menter

    ; we doing this the stupid way
    pushlea eax, dword [ebp + .working_slice]
    mov eax, dword [ebp + .token_ptr]
    pushlea eax, dword [eax + token.slice]
    call slice_alloc_copy
    popn 2

    mov eax, dword [ebp + .dest]
    pushlea dword [eax + obj.extra]
    push number_input_format
    push dword [ebp + .working_slice + slice.ptr]
    call sscanf
    popn 3

    mov eax, dword [ebp + .dest]
    mov dword [eax + obj.tag], object_tag.integer

    mov eax, 0
    leave
    ret

    ; normal code after
    mov dword [ebp + .radix], 0
    mov dword [ebp + .exactness], 0

    mov esi, dword [ebp + .token_ptr]
    cmp dword [esi + token.tag], token_tag.number
    assert e, "Parse number was not passed a number token"

    mprintfn "token length: %d", dword [esi + token.slice + slice.len]

    pushlea eax, [esi + token.slice]
    pushlea eax, [ebp + .working_slice]
    call slice_copy
    popn 2

    mov edi, dword [esi + token.slice + slice.ptr]
    mov al, byte [edi]

    .parse_specifier:
    cmp al, '#'
    jne .specifiers_done
        mov al, byte [edi + 1]
        or al, 0b1000000 ; lowercase letters

        cmp al, 'e'
        je .spec_exact
        cmp al, 'i'
        je .spec_inexact
        cmp al, 'b'
        je .spec_binary
        cmp al, 'o'
        je .spec_octal
        cmp al, 'd'
        je .spec_decimal
        cmp al, 'x'
        je .spec_hexadecimal
        ; else error
        jmp .error_bad_specifier

        .spec_exact:
            cmp dword [ebp + .exactness], 0
            je .error_bad_specifier
            mov dword [ebp + .exactness], 1

        .spec_inexact:
            cmp dword [ebp + .exactness], 0
            je .error_bad_specifier
            mov dword [ebp + .exactness], 2

        .spec_binary:
            cmp dword [ebp + .radix], 0
            je .error_bad_specifier
            mov dword [ebp + .radix], 2

        .spec_octal:
            cmp dword [ebp + .radix], 0
            je .error_bad_specifier
            mov dword [ebp + .radix], 8

        .spec_decimal:
            cmp dword [ebp + .radix], 0
            je .error_bad_specifier
            mov dword [ebp + .radix], 10

        .spec_hexadecimal:
            cmp dword [ebp + .radix], 0
            je .error_bad_specifier
            mov dword [ebp + .radix], 16
            
        .finish_specifier:
        add edi, 2
        jmp .parse_specifier
    .specifiers_done:

    cmp dword [ebp + .radix], 0
    jne .has_radix
        mov dword [ebp + .radix], 10 ; default to decimal
    .has_radix:

    sub edi, dword [ebp + .working_slice + slice.ptr]
    push edi
    pushlea eax, [ebp + .working_slice]
    call slice_advance
    popn 2

    pushlea eax, [ebp + .real]
    push dword [ebp + .error_ptr]
    push dword [ebp + .radix]
    pushlea eax, dword [ebp + .working_slice]
    call parse_number_real
    popn 4
    cmp eax, 0
    jne .error


    mov eax, dword [ebp + .real + real.ureal + ureal.tag]
    cmp eax, ureal_tag.ureal_integer
    je .integer
    cmp eax, ureal_tag.ureal_decimal
    je .decimal
    cmp eax, ureal_tag.ureal_rational
    je .rational
    die "Corrupted memory"

    .integer:
        mov esi, dword [ebp + .real + real.ureal + ureal_integer.digits + slice.ptr]
        mov ecx, dword [ebp + .real + real.ureal + ureal_integer.digits + slice.len]
        mprintfn "digits: %d", ecx
        mov edx, 0
        .int_parse_loop:    
            lodsb
            movzx eax, al
            push eax
            call parse_number_digit_to_value
            popn 1

            imul edx, dword [ebp + .radix]
            jc .error_overflow
            add edx, eax
            jc .error_overflow
        loop .int_parse_loop
        imul edx, dword [ebp + .real + real.sign]

        push edx
        pushlea eax, dword [ebp + .dest]
        call integer_new
        popn 2

        mov eax, 0
        leave
        ret

    .decimal:
    die "Decimal parsing is unimplemented"

    .rational:
    die "Rational parsing is unimplemented"
    
    .error_overflow:
    mov esi, dword [ebp + .error_ptr]
    mov eax, dword [ebp + .token_ptr]
    mov eax, dword [eax + token.slice + slice.ptr]
    mov dword [esi + syntax_error.location], eax
    mov dword [esi + syntax_error.message], syntax_error_overflow
    jmp .error

    .error_bad_specifier:
    mov esi, dword [ebp + .error_ptr]
    mov dword [esi + syntax_error.location], edi
    mov dword [esi + syntax_error.message], syntax_error_bad_specifier
    jmp .error

    .error:
    mov eax, 1
    leave
    ret
    %pop


parse_number_sign:
    %push
    marg .source:dword, .dest:dword
    menter

    mov esi, dword [ebp + .source]
    mov edi, dword [esi + slice.ptr]

    mov al, byte [edi]
    cmp al, '-'
    je .sign_minus
    cmp al, '+'
    je .sign_plus
    jmp .sign_done

    .sign_minus:
    mov eax, dword [ebp + .dest]
    mov dword [eax], -1
    .sign_plus:
    push 1
    push dword [ebp + .source]
    call slice_advance
    popn 2
    .sign_done:

    mov eax, 0
    leave
    ret
    %pop

parse_number_real:
    %push
    marg .source:dword, .radix:dword, .error_ptr:dword, .dest:dword
    menter

    ; mov eax, dword [ebp + .dest]
    ; pushlea eax, dword [eax + real.sign]
    ; push dword [ebp + .source]
    ; call parse_number_sign
    ; popn 2

    mov eax, dword [ebp + .dest]
    pushlea eax, dword [eax + real.ureal]
    push dword [ebp + .error_ptr]
    push dword [ebp + .radix]
    push dword [ebp + .source]
    call parse_number_ureal
    popn 4

    leave
    ret
    %pop


parse_number_ureal:
    %push
    marg .slice_ptr:dword, .radix:dword, .error_ptr:dword, .dest:dword
    mlocal .digits1:slice_size
    menter

    push dword [ebp + .digits1]
    push dword [ebp + .error_ptr]
    push dword [ebp + .radix]
    push dword [ebp + .slice_ptr]
    call parse_number_uinteger
    popn 4
    cmp eax, 0
    jne .error

    mov edi, dword [ebp + .digits1 + slice.ptr]

    mov esi, dword [ebp + .slice_ptr]
    mov eax, dword [esi + slice.len]
    mov ebx, dword [ebp + .digits1 + slice.len]
    cmp ebx, eax
    jae .integer

    mov eax, dword [esi + slice.ptr]
    add eax, dword [esi + slice.len]

    mov bl, byte [eax]
    cmp bl, '/'
    je .rational
    cmp bl, '.'
    je .decimal

    mov edi, eax
    jmp .error_bad_digit

    .decimal:
        mov eax, dword [ebp + .digits1 + slice.len]
        inc eax
        push eax
        push dword [ebp + .slice_ptr]
        call slice_advance
        popn 2

        mov edi, dword [ebp + .dest]

        mov dword [edi + ureal.tag], ureal_tag.ureal_decimal

        pushlea eax, dword [ebp + .digits1]
        pushlea eax, dword [edi + ureal_decimal.whole_digits]
        call slice_copy
        popn 2

        pushlea eax, dword [edi + ureal_decimal.fract_digits]
        push dword [ebp + .error_ptr]
        push dword [ebp + .radix]
        push dword [ebp + .slice_ptr]
        call parse_number_uinteger
        popn 4
        cmp eax, 0
        jne .error

        mov edi, dword [ebp + .dest]
        ; TODO: parse decimal suffix
        mov dword [edi + ureal_decimal.expt], 0

        pushlea eax, dword [edi + ureal_decimal.fract_digits]
        push dword [ebp + .slice_ptr]
        call slice_advance
        popn 2

        jmp .finish

    .rational:
        mov eax, dword [ebp + .digits1 + slice.len]
        inc eax
        push eax
        push dword [ebp + .slice_ptr]
        call slice_advance
        popn 2

        mov edi, dword [ebp + .dest]
        mov dword [edi + ureal.tag], ureal_tag.ureal_rational


        pushlea eax, dword [ebp + .digits1]
        pushlea eax, dword [edi + ureal_rational.num_digits]
        call slice_copy
        popn 2

        pushlea eax, dword [edi + ureal_rational.den_digits]
        push dword [ebp + .error_ptr]
        push dword [ebp + .radix]
        push dword [ebp + .slice_ptr]
        call parse_number_uinteger
        popn 4
        cmp eax, 0
        jne .error

        mov edi, dword [ebp + .dest]
        pushlea eax, dword [edi + ureal_rational.den_digits]
        push dword [ebp + .slice_ptr]
        call slice_advance
        popn 2

        jmp .finish

    .integer:
        mov edx, dword [ebp + .dest]
        mov dword [edx + ureal.tag], ureal_tag.ureal_integer

        pushlea eax, dword [ebp + .digits1]
        pushlea eax, dword [edx + ureal_integer.digits]
        call slice_copy
        popn 2

        push dword [ebp + .digits1 + slice.len]
        push dword [ebp + .slice_ptr]
        call slice_advance
        popn 2

        jmp .finish

    .finish:
        mov eax, dword [ebp + .slice_ptr]
        mov edi, dword [eax + slice.ptr]
        cmp dword [eax + slice.len], 0
        jnz .error_bad_digit
        mprintfn "finish number"


        mov eax, 0
        leave
        ret

    .error_bad_digit:
    mov esi, dword [ebp + .error_ptr]
    mov dword [esi + syntax_error.location], edi
    mov dword [esi + syntax_error.message], syntax_error_bad_digit

    .error:
    mov eax, 1
    leave
    ret
    %pop

; Arguments:
; - slice_ptr: pointer to the source slice
; - radix: radix of number
; - error_ptr: pointer to syntax_error object
; - dest: pointer to slice to put digits into
parse_number_uinteger:
    %push
    marg .slice_ptr:dword, .radix:dword, .error_ptr:dword, .dest:dword
    mlocal .end:dword
    menter

    mov edx, dword [ebp + .slice_ptr]
    mov edi, dword [edx + slice.ptr]

    mov eax, edi
    add eax, dword [edx + slice.len]
    mov dword [ebp + .end], eax

    mov esi, edi ; use esi to mark beginning of digits

    .parse_digit:
    cmp edi, dword [ebp + .end]
    jae .digits_done

    movzx eax, byte [edi]

    movzx ebx, byte [digit_radix_table + eax]

    cmp dword [ebp + .radix], ebx
    jge .good_digit
        cmp ebx, -1
        je .digits_done
        jmp .error_bad_digit
    .good_digit:
    inc edi
    jmp .parse_digit

    .digits_done:
    push edi
    push esi
    pushlea eax, [ebp + .dest]
    call slice_from_ptrs
    popn 3

    mov eax, 0
    leave
    ret

    .error_bad_digit:
    mov esi, dword [ebp + .error_ptr]
    mov dword [esi + syntax_error.location], edi
    mov dword [esi + syntax_error.message], syntax_error_bad_digit

    .error:
    mov eax, 1
    leave
    ret
    %pop


%endif


