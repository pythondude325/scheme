%ifndef _PARSER_ASM
%define _PARSER_ASM

%include "common.asm"
%include "lexer.asm"
%include "objects.asm"
%include "interner.asm"
%include "parse_number.asm"

segment .rodata
    syntax_error_format_string: db "%s:%d:%d: syntax error: %s",10,0

    syntax_error_bad_improper_list: db "Illegal use of improper list",0
    syntax_error_bad_specifier: db "Bad specifier in numeric literal",0
    syntax_error_bad_digit: db "Invalid digit in numeric literal",0
    syntax_error_overflow: db "Overflow in numeric literal",0

struc syntax_error
    .message: resd 1
    .location: resd 1
endstruc

segment .text

print_syntax_error:
    %push
    marg .lex:dword, .error:dword, .filename:dword
    mlocal .file_location:file_location_size
    menter

    mov esi, dword [ebp + .error]
    cmp dword [esi + syntax_error.message], 0
    assert ne, "Tried to print an invalid error"

    
    mov eax, dword [esi + syntax_error.location]
    sub eax, sample_source
    ; mprintfn "source offset: %d", eax

    pushlea eax, dword [ebp + .file_location]
    push dword [esi + syntax_error.location]
    push dword [ebp + .lex]
    call lexer_get_location
    popn 3

    mov esi, dword [ebp + .error]
    ; mprintfn "error message: %08x", dword [esi + syntax_error.message]

    push dword [esi + syntax_error.message]
    push dword [ebp + .file_location + file_location.column]
    push dword [ebp + .file_location + file_location.line]
    push dword [ebp + .filename]
    push syntax_error_format_string
    push dword [stderr]
    call fprintf
    popn 6

    leave
    ret
    %pop

; Arguments:
; - lex: pointer to lexer
; - dest: pointer to object to leave
parse_sexpr:
    %push
    marg .lex:dword, .error_ptr:dword, .dest:dword
    mlocal .token:token_size, .list_tail:dword, .items:vector_size
    menter

    pushlea eax, [ebp + .token]
    push dword [ebp + .lex]
    call lexer_peek_token
    popn 2

    mov eax, dword [ebp + .token + token.tag]
    cmp eax, token_tag.symbol
    je .symbol
    cmp eax, token_tag.list_begin
    je .list
    cmp eax, token_tag.number
    je .number
    cmp eax, token_tag.boolean_true
    je .true
    cmp eax, token_tag.boolean_false
    je .false
    ; cmp eax, token_tag.vector_begin
    ; je .vector

    die "unimplemented/invalid syntax"

    .symbol:
        pushlea eax, [ebp + .token + token.slice]
        push dword [ebp + .dest]
        call symbol_new_from_slice
        popn 2

        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_advance
        popn 2

        mov eax, 0
        leave
        ret

    .number:
        push dword [ebp + .dest]
        push dword [ebp + .error_ptr]
        pushlea eax, dword [ebp + .token]
        call parse_number
        popn 3
        cmp eax, 0
        jne .error

        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_advance
        popn 2

        jmp .finish

    .list:
        mov eax, dword [ebp + .dest]
        mov dword [ebp + .list_tail], eax

        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_advance
        popn 2

        .list_loop:
        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_peek_token
        popn 2

        mov eax, dword [ebp + .token + token.tag]
        cmp eax, token_tag.list_end
        je .list_end
        cmp eax, token_tag.list_improper
        je .list_improper

        push dword [ebp + .list_tail]
        call pair_new
        popn 1

        push dword [ebp + .list_tail]
        call pair_car
        popn 1

        push eax
        push dword [ebp + .error_ptr]
        push dword [ebp + .lex]
        call parse_sexpr
        popn 3
        cmp eax,0
        jne .error

        push dword [ebp + .list_tail]
        call pair_cdr
        popn 1
        mov dword [ebp + .list_tail], eax
        jmp .list_loop


        .list_end:
        push dword [ebp + .list_tail]
        call null_new
        popn 1

        ; advance past the closing paren
        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_advance
        popn 2

        jmp .finish

        .list_improper:
        ; advance past the dot
        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_advance
        popn 2

        ; parse the list tail expression
        push dword [ebp + .list_tail]
        push dword [ebp + .error_ptr]
        push dword [ebp + .lex]
        call parse_sexpr
        popn 3
        cmp eax, 0
        jne .error

        ; peek for the closing paren
        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_peek_token
        popn 2

        cmp dword [ebp + .token + token.tag], token_tag.list_end
        je .good_improper_list
            mov esi, dword [ebp + .error_ptr]
            mov eax, dword [ebp + .token + token.slice + slice.ptr]
            mov dword [esi + syntax_error.location], eax
            mov dword [esi + syntax_error.message], syntax_error_bad_improper_list
            jmp .error
        .good_improper_list:

        ; advance past the closing paren
        pushlea eax, [ebp + .token]
        push dword [ebp + .lex]
        call lexer_advance
        popn 2

        jmp .finish

    .true:
        push dword [ebp + .dest]
        call boolean_true_new
        popn 1
        jmp .finish

    .false:
        push dword [ebp + .dest]
        call boolean_false_new
        popn 1
        jmp .finish

    .finish:
    mov eax, 0
    leave
    ret

    .error:
    mov eax, 1
    leave
    ret
    
    %pop



%endif