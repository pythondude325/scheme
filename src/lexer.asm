%ifndef _LEXER_ASM
%define _LEXER_ASM

%include "common.asm"
%include "slice.asm"

struc lexer
	.source: resb slice_size
	.source_ptr equ .source + slice.ptr

	.current_pos: resd 1
endstruc

; positive values are tokens
; negative values are special non-tokens
token_tag:
	.list_begin equ 0
	.list_end equ 1
	.vector_begin equ 2
	.quote equ 3
	.quasiquote equ 4
	.unquote equ 5
	.unquote_splicing equ 6
	.symbol equ 7
	.boolean_true equ 8
	.boolean_false equ 9
	.character equ 10
	.number equ 11
	.list_improper equ 12

	.eof equ -2
	.error equ -1

struc token
	.tag: resd 1
	.slice: resb slice_size
endstruc



segment .text

lexer_char_is_whitespace:
	%push
	marg .char:dword
	menter

	mov eax, dword [ebp + .char]

	cmp al, ' ' ; space
	je .whitespace

	cmp al, 9 ; tab
	je .whitespace

	cmp al, 10 ; newline
	je .whitespace

	; not whitespace
	mov eax, 0
	leave
	ret

	.whitespace:
	mov eax, 1
	leave
	ret
	%pop

lexer_char_is_delimiter:
	%push
	marg .char:dword
	menter

	mov eax, dword [ebp + .char]
	
	cmp al, '('
	je .found_delimiter
	cmp al, ')'
	je .found_delimiter
	cmp al, '"'
	je .found_delimiter
	cmp al, ';'
	je .found_delimiter

	; while \0 isn't actually a delimiter, we don't want to overrun the source
	; buffer.
	cmp al, 0
	je .found_delimiter

	push dword [ebp + .char]
	call lexer_char_is_whitespace
	popn 1
	leave
	ret

	.found_delimiter:
	mov eax, 1
	leave
	ret

; Take a pointer and return a pointer to the next delimiter
lexer_seek_delimiter:
	%push
	marg .start:dword
	menter

	.next_char:
		inc dword [ebp + .start]
	.loop_cond:
	mov eax, dword [ebp + .start]
	push dword [eax]
	call lexer_char_is_delimiter
	popn 1
	cmp eax, 1
	jne .next_char

	mov eax, dword [ebp + .start]
	leave
	ret
	%pop

; Arguments:
; - dest: ptr to lexer to initialize
; - source: cstr of source code to tokenize
lexer_new:
	%push
	marg .dest:dword, .source:dword
	menter

	mov eax, dword [ebp + .dest]
	mov ebx, dword [ebp + .source]
	mov dword [eax + lexer.current_pos], ebx
	
	push dword [ebp + .source]
	pushlea eax, [eax + lexer.source]
	call slice_from_cstr
	popn 2

	mov eax, 0
	leave
	ret
	%pop

struc file_location
	.line: resd 1
	.column: resd 1
endstruc

lexer_get_location:
	%push
	marg .lex:dword, .location:dword, .dest:dword
	mlocal .lines:dword, .last_newline:dword
	menter

	mov eax, dword [ebp + .lex]
	mov esi, dword [eax + lexer.source_ptr]

	mov dword [ebp + .lines], 0
	mov dword [ebp + .last_newline], esi

	push dword [ebp + .location]
	pushlea eax, dword [eax + lexer.source]
	call slice_check_bounds
	popn 2
	cmp eax, 0
	assert e, "File location out of bounds"

	mov ecx, dword [ebp + .location]
	mov eax, dword [ebp + .lex]
	sub ecx, dword [eax + lexer.source_ptr]
	cmp ecx, 0
	je .done
	cld
	.loop:
		lodsb
		cmp al, 10
		jne .newline
			inc dword [ebp + .lines]
			mov dword [ebp + .last_newline], esi
		.newline:
	loop .loop 

	.done:
	mov edi, dword [ebp + .dest]

	mov eax, dword [ebp + .lines]
	inc eax
	mov dword [edi + file_location.line], eax

	mov eax, esi
	sub eax, dword [ebp + .last_newline]
	inc eax
	mov dword [edi + file_location.column], eax

	mov eax, 0
	leave
	ret
	%pop

lexer_init_token:
	%push
	marg .dest:dword, .tag:dword, .ptr:dword, .len:dword
	menter

	mov eax, dword [ebp + .dest]
	mov ebx, dword [ebp + .tag]
	mov dword [eax + token.tag], ebx
	mov ebx, dword [ebp + .ptr]
	mov dword [eax + token.slice + slice.ptr], ebx
	mov ebx, dword [ebp + .len]
	mov dword [eax + token.slice + slice.len], ebx

	leave
	ret
	%pop

lexer_peek_token:
	%push
	marg .lex:dword, .tok:dword
	mlocal .current_pos:dword
	menter

	mov eax, dword [ebp + .lex]
	mov esi, dword [eax + lexer.current_pos]

	.scan_char:
	mov dword [ebp + .current_pos], esi

	push esi
	mov eax, dword [ebp + .lex]
	pushlea eax, dword [eax + lexer.source]
	call slice_check_bounds
	popn 2

	mov esi, dword [ebp + .current_pos]

	cmp eax, 0
	jne .eof

	movzx eax, byte [esi]
	; mov eax, [.jump_table + 4*eax]
	jmp [.jump_table + 4*eax]

	segment .rodata
	.jump_table:
		dd .eof,.ill,.ill,.ill, .ill,.ill,.ill,.ill ; 0x00
		dd .ill,.ws,.ws,.ill, .ill,.ws,.ill,.ill ; 0x08
		times 16 dd .ill ; 0x10
		dd .ws,.sym,.string,.hash, .sym,.sym,.sym,.quote ; 0x20
		dd .list_begin,.list_end,.sym,.num, .unquote,.num,.num,.sym ; 0x28
		dd .num,.num,.num,.num, .num,.num,.num,.num ; 0x30
		dd .num,.num,.sym,.comment, .sym,.sym,.sym,.sym ; 0x38
		dd .ill,.sym,.sym,.sym, .sym,.sym,.sym,.sym ; 0x40
		times 8 dd .sym ; 0x48
		times 8 dd .sym ; 0x50
		dd .sym,.sym,.sym,.ill, .ill,.ill,.sym,.sym ; 0x58
		dd .quasiquote,.sym,.sym,.sym, .sym,.sym,.sym,.sym ; 0x60
		times 8 dd .sym ; 0x68
		times 8 dd .sym ; 0x70
		dd .sym,.sym,.sym,.ill, .ill,.ill,.sym,.ill ; 0x78
		times 128 dd .ill ; mutlibyte characters
	segment .text

	.eof:
		mpush dword [ebp + .tok], token_tag.eof, esi, 0
		call lexer_init_token
		popn 4
		jmp .finish

	.ws: ; whitespace
		inc esi ; advance to next character and try again
		jmp .scan_char

	.sym: ; symbol

		segment .rodata
		; table for which characters are allowed in identifiers and which are not
		; I figured this out from the grammar in R5RS § 7.1.1
		; 0 for not allowed
		; 1 for allowed as subsequent
		; 2 for allowed as initial
		.ident_table:
			times 16 db 0 ; 0x0_
			times 16 db 0 ; 0x1_
			db 0,2,0,0, 2,2,2,0, 0,0,2,1, 0,1,1,2 ; 0x2_
			db 1,1,1,1, 1,1,1,1, 1,1,2,0, 2,2,2,2 ; 0x3_
			db 1,2,2,2, 2,2,2,2, 2,2,2,2, 2,2,2,2 ; 0x4_
			db 2,2,2,2, 2,2,2,2, 2,2,2,0, 0,0,2,2 ; 0x5_
			db 0,2,2,2, 2,2,2,2, 2,2,2,2, 2,2,2,2 ; 0x6_
			db 2,2,2,2, 2,2,2,2, 2,2,2,0, 0,0,2,0 ; 0x7_
			times 128 dd 0 ; multibyte characters
		segment .text

		mov edi, esi

		movzx eax, byte [edi]
		mov al, byte [.ident_table + eax]
		cmp al, 2
		assert nb, "Illegal start symbol"

		.continue_symbol:
		inc edi
		movzx eax, byte [edi]
		mov al, byte [.ident_table + eax]
		cmp al, 1
		jnb .continue_symbol

		; symbol is finished, start at edi, end at esi
		mov eax, dword [ebp + .tok]
		mov dword [eax + token.tag], token_tag.symbol

		push edi
		push esi
		pushlea eax, [eax + token.slice]
		call slice_from_ptrs
		popn 3
		
		jmp .finish

	.peculiar_sym:
		cmp byte [esi], '-'
		jne .not_minus
			mpush dword [ebp + .tok], token_tag.symbol, esi, 1
			call lexer_init_token
			popn 4
			jmp .finish
		.not_minus:
		cmp byte [esi], '+'
		jne .not_plus
			mpush dword [ebp + .tok], token_tag.symbol, esi, 1
			call lexer_init_token
			popn 4
			jmp .finish
		.not_plus:
		cmp byte [esi], '.'
		jne .ill
		cmp byte [esi + 1], '.'
		jne .ill
		cmp byte [esi + 2], '.'
		jne .ill
		mpush dword [ebp + .tok], token_tag.symbol, esi, 3
		call lexer_init_token
		popn 4
		jmp .finish

	.num: ; numeric literal
		push esi
		call lexer_seek_delimiter
		popn 1
		sub eax, esi

		; Filter out the numeric false positives
		cmp eax, 3
		jne .not_elipses
			cmp byte [esi], '.'
			jne .not_elipses
			cmp byte [esi + 1], '.'
			jne .not_elipses
			cmp byte [esi + 2], '.'
			je .peculiar_sym
		.not_elipses:

		cmp eax, 1
		jne .not_1_long
			cmp byte [esi], '-'
			je .peculiar_sym
			cmp byte [esi], '+'
			je .peculiar_sym
			cmp byte [esi], '.'
			je .list_improper
		.not_1_long:
	
		mpush dword [ebp + .tok], token_tag.number, esi, eax
		call lexer_init_token
		popn 4
		jmp .finish

	.list_begin:
		mpush dword [ebp + .tok], token_tag.list_begin, esi, 1
		call lexer_init_token
		popn 4
		jmp .finish

	.list_end:
		mpush dword [ebp + .tok], token_tag.list_end, esi, 1
		call lexer_init_token
		popn 4
		jmp .finish

	.list_improper:
		mpush dword [ebp + .tok], token_tag.list_improper, esi, 1
		call lexer_init_token
		popn 4
		jmp .finish

	.comment:
		; TODO: make sure this logic is correct
		.comment_next_char:
			inc esi
			mov al, byte [esi]
			cmp al, 0
			je .comment_finished ; break on null terminator
			cmp al, 10
			jne .comment_next_char
		
		.comment_finished:
		dec esi
		jmp .ws

	.hash: ; hash
		mov al, byte [esi + 1]
		cmp al, '\'
		je .character
		cmp al, 't'
		je .true
		cmp al, 'f'
		je .false
		cmp al, '('
		je .vector_begin
		; all different types of number prefixes
		cmp al, 'e'
		je .num
		cmp al, 'i'
		je .num
		cmp al, 'b'
		je .num
		cmp al, 'o'
		je .num
		cmp al, 'd'
		je .num
		cmp al, 'x'
		je .num
		
		; ERROR because of invalid token
		; TODO: maybe flag the second char as invalid
		; inc esi
		jmp .ill

	.character: ; character literal
		push esi
		call lexer_seek_delimiter
		popn 1
		sub eax, esi
		
		; TODO: check if this is a legal charater

		mpush dword [ebp + .tok], token_tag.character, esi, eax
		call lexer_init_token
		popn 4
		jmp .finish

	.true: ; boolean literal true
		mpush dword [ebp + .tok], token_tag.boolean_true, esi, 2
		call lexer_init_token
		popn 4
		jmp .finish

	.false: ; boolean literal false
		mpush dword [ebp + .tok], token_tag.boolean_false, esi, 2
		call lexer_init_token
		popn 4
		jmp .finish

	.vector_begin: ; #( token
		mpush dword [ebp + .tok], token_tag.vector_begin, esi, 2
		call lexer_init_token
		popn 4
		jmp .finish

	.string: ; string literal
		; TODO: implement this one
		die "not yet"

	.quote: ; quote abbreviation (')
		mpush dword [ebp + .tok], token_tag.quote, esi, 1
		call lexer_init_token
		popn 4
		jmp .finish

	.quasiquote: ; quasiquote abbreviation (`)
		mpush dword [ebp + .tok], token_tag.quasiquote, esi, 1
		call lexer_init_token
		popn 4
		jmp .finish

	.unquote: ; unquote abbreviation (,)
		mov al, byte [esi + 1]
		cmp al, '@'
		je .unquote_splicing
		
		mpush dword [ebp + .tok], token_tag.unquote, esi, 1
		call lexer_init_token
		popn 4
		jmp .finish

	.unquote_splicing: ; unquote-splicing abbreviation (,@)
		mpush dword [ebp + .tok], token_tag.unquote_splicing, esi, 2
		call lexer_init_token
		popn 4
		jmp .finish

	.ill: ; illegal char
		mpush dword [ebp + .tok], token_tag.error, esi, 1
		call lexer_init_token
		popn 4
		jmp .finish

	.finish:
	leave
	ret
	%pop

lexer_advance:
	%push
	marg .lex:dword, .token_ptr:dword
	menter

	mov eax, dword [ebp + .token_ptr]
	mov ebx, dword [eax + token.slice + slice.ptr]
	add ebx, dword [eax + token.slice + slice.len]
	mov eax, dword [ebp + .lex]
	mov dword [eax + lexer.current_pos], ebx

	leave
	ret
	%pop



%endif