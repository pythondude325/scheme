%ifndef _UTF8_ASM
%define _UTF8_ASM


utf8_length:
    %push
    marg .codepoint:dword
    menter

    mov eax, 32
    lzcnt ebx, dword [ebp + .codepoint]
    sub eax, ebx

    cmp eax, 7
    ja .not_1_byte
        mov eax, 1
        leave
        ret
    .not_1_byte:

    sub eax, 6
    cmp eax, 5
    ja .not_2_bytes
        mov eax, 2
        leave
        ret
    .not_2_bytes:
    
    sub eax, 6
    cmp eax, 4
    ja .not_3_bytes
        mov eax, 3
        leave
        ret
    .not_3_bytes:

    sub eax, 6
    cmp eax, 3
    ja .not_4_bytes
        mov eax, 4
        leave
        ret
    .not_4_bytes:

    die "Invalid unicode codepoint"
    %pop

utf8_encode:
    %push
    marg .codepoint:dword
    menter




    leave
    ret
    %pop







%endif