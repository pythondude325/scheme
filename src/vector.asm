%ifndef _VECTOR_ASM
%define _VECTOR_ASM

%include "common.asm"
%include "alloc.asm"

segment .text

extern free
extern memcpy

struc vector
    .ptr: resd 1
    .len: resd 1
    .capacity: resd 1
    .element_size: resd 1

    .default_capacity equ 32
endstruc

vec_new:
    %push
    marg .vec:dword, .element_size:dword
    menter
    
    mov eax, dword [ebp + .element_size]
    mov ebx, dword [ebp + .element_size]
    dec ebx
    and eax, ebx
    cmp eax, 0
    assert e, "Vector element size must be a power of two"

    mov eax, dword [ebp + .element_size]
    shl eax, ilog2(vector.default_capacity)
    push eax
    call safe_alloc
    popn 1

    mov edi, dword [ebp + .vec]
    mov dword [edi + vector.ptr], eax
    mov dword [edi + vector.len], 0
    mov dword [edi + vector.capacity], vector.default_capacity
    mov eax, dword [ebp + .element_size]
    mov dword [edi + vector.element_size], eax

    leave
    ret
    %pop

vec_delete:
    %push
    marg .vec:dword
    menter

    mov edi, dword [ebp + .vec]
    push dword [edi + vector.ptr]
    mov dword [edi + vector.ptr], 0
    call free
    popn 1
    
    leave
    ret
    %pop

vec_realloc:
    %push
    marg .vec:dword, .new_capacity:dword
    menter

    mov edi, dword [ebp + .vec]
    mov eax, dword [ebp + .new_capacity]
    mov dword [edi + vector.capacity], eax

    tzcnt ecx, dword [edi + vector.element_size]
    shl eax, cl

    push eax
    push dword [edi + vector.ptr]
    call safe_realloc
    popn 2

    mov edi, dword [ebp + .vec]
    mov dword [edi + vector.ptr], eax
    
    leave
    ret
    %pop

; Arugments:
; - vec: pointer to a vector
; - n: number of elements to extend the vector by
; Returns: a pointer to the first extended element
vec_extend:
    %push
    marg .vec:dword, .n:dword
    menter

    mov edi, dword [ebp + .vec]
    mov eax, dword [edi + vector.len]
    add eax, dword [ebp + .n]
    cmp eax, dword [edi + vector.capacity]
    jbe .good_capacity
        mov ebx, dword [edi + vector.capacity]

        .more_capacity:
        shl ebx, 1
        mov eax, dword [edi + vector.len]
        add eax, dword [ebp + .n]
        cmp eax, ebx
        ja .more_capacity

        push ebx
        push dword [ebp + .vec]
        call vec_realloc
        popn 2
    .good_capacity:

    mov edi, dword [ebp + .vec]
    mov eax, dword [edi + vector.len]
    tzcnt ecx, dword [edi + vector.element_size]
    shl eax, cl

    mov ebx, dword [edi + vector.ptr]
    lea eax, [ebx + eax]

    mov ebx, dword [ebp + .n]
    add dword [edi + vector.len], ebx

    leave
    ret
    %pop

vec_pop:
    %push
    marg .vec:dword
    menter

    mov edi, dword [ebp + .vec]
    mov eax, dword [edi + vector.len]
    tzcnt ecx, dword [edi + vector.capacity]
    sub ecx, 2
    shr eax, cl
    cmp eax, 0
    ja .good_capacity
        cmp dword [edi + vector.capacity], vector.default_capacity
        jbe .good_capacity

        mov eax, dword [edi + vector.capacity]
        shr eax, 1
        
        push eax
        push dword [ebp + .vec]
        call vec_realloc
        popn 2
    .good_capacity:

    mov edi, dword [ebp + .vec]
    cmp dword [edi + vector.len], 0
    je .no_items

    dec dword [edi + vector.len]

    .no_items:
    leave
    ret
    %pop

%endif