%ifndef _WRITER_ASM
%define _WRITER_ASM

segment .rodata
    writer_strs:
        .null: db "()",0
        .open_paren: db "(",0
        .close_paren: db ")",0
        .dot: db ". ",0
        .symbol_format: db "%s",0
        .integer_format: db "%d",0
        .space: db " ",0

segment .text

extern fprintf

write_obj_to_stdout:
    %push
    marg .fp:dword, .object_ptr:dword
    mlocal .current_object_ptr:dword
    menter

    mov esi, dword [ebp + .object_ptr]
    movzx eax, word [esi + obj.tag]
    cmp ax, object_tag.null
    je .null
    cmp ax, object_tag.symbol
    je .symbol
    cmp ax, object_tag.pair
    je .list
    cmp ax, object_tag.integer
    je .integer

    die "unimplemented object write"

    .null:
        push writer_strs.null
        push dword [ebp + .fp]
        call fprintf
        popn 2
        leave
        ret

    .symbol:
        mov eax, dword [esi + obj.extra]

        push eax
        call interner_resolve_cstr
        popn 1

        push eax
        push dword writer_strs.symbol_format
        push dword [ebp + .fp]
        call fprintf
        popn 3
        leave
        ret

    .list:
        mov eax, dword [ebp + .object_ptr]
        mov dword [ebp + .current_object_ptr], eax

        ; print the opening parenthesis for the list
        push writer_strs.open_paren
        push dword [ebp + .fp]
        call fprintf
        popn 2

        ; TODO: This loop needs to be restructured
        .list_loop:
        push dword [ebp + .current_object_ptr]
        call pair_car
        popn 1

        push eax
        push dword [ebp + .fp]
        call write_obj_to_stdout
        popn 1

        push dword [ebp + .current_object_ptr]
        call pair_cdr
        popn 1

        mov dword [ebp + .current_object_ptr], eax

        mov bx, word [eax + obj.tag]

        cmp bx, object_tag.null
        je .skip_space
            push writer_strs.space
            push dword [ebp + .fp]
            call fprintf
            popn 2
        .skip_space:

        cmp bx, object_tag.null
        je .finish_list
        cmp bx, object_tag.pair
        je .list_loop
        ; else improper list

        .finish_improper_list:
        push writer_strs.dot
        push dword [ebp + .fp]
        call fprintf
        popn 2

        push dword [ebp + .current_object_ptr]
        push dword [ebp + .fp]
        call write_obj_to_stdout
        popn 1
        jmp .finish_list

        .finish_list:
        push writer_strs.close_paren
        push dword [ebp + .fp]
        call fprintf
        popn 2
        leave
        ret

    .integer:
        mov eax, dword [esi + obj.extra]

        push eax
        push writer_strs.integer_format
        push dword [ebp + .fp]
        call fprintf
        popn 3
        leave
        ret

    %pop

%endif