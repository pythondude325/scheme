%ifndef _SIMD_ASM
%define _SIMD_ASM

%macro splat 2
	movss %1, %2
	shufps %1, %1, 0
%endmacro

%endif