%include "common.asm"
%include "objects.asm"
%include "gc.asm"
%include "lexer.asm"
%include "interner.asm"
%include "utf8.asm"
%include "parser.asm"
%include "writer.asm"

segment .rodata
	filename: db "<input>",0

	test_scm:
	incbin "test.scm"
	db 0

segment .bss
	test_lexer: resb lexer_size
	test_token: resb token_size

	sample_source: resb 1024

segment .text
	global main
	extern fgets

main:
	%push
	marg .argc:dword, .argv:dword
	mlocal .table:hashtable_size, .local_object:obj_size, .my_slice:slice_size, \
		.file_location:file_location_size, .temp:dword, \
		.syntax_error:syntax_error_size, .result:obj_size
	menter
	; ********** CODE STARTS HERE **********

	push dword [stdin]
	push 1023
	push sample_source
	call fgets
	popn 3	

	call gc_init
	call interner_init

	push dword sample_source
	push dword test_lexer
	call lexer_new
	popn 2

	pushlea eax, [ebp + .local_object]
	pushlea eax, [ebp + .syntax_error]
	push dword test_lexer
	call parse_sexpr
	popn 3

	cmp eax,0
	je .no_error
		push filename
		pushlea eax, [ebp + .syntax_error]
		push test_lexer
		call print_syntax_error
		popn 3

		jmp .finish
	.no_error:

	pushlea eax, [ebp + .local_object]
	push dword [stdout]
	call write_obj_to_stdout
	popn 2
	mprintfn ""

	.finish:
	call gc_clear
	call gc_collect

	call interner_deinit
	call gc_deinit

	; *********** CODE ENDS HERE ***********
	mov		eax, 0
	leave
	ret
	%pop