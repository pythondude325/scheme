%ifndef _SLICE_ASM
%define _SLICE_ASM

%include "common.asm"
%include "alloc.asm"

struc slice
	.ptr: resd 1
	.len: resd 1
endstruc

segment .text
	extern free

; arguments:
; - slice_p: pointer to slice to write to
; - size: number of bytes to allocate
; returns: 0 on success, other on failure
slice_new:
	%push
	marg .slice_p:dword, .size:dword
	mlocal .allocation:dword
	menter

	push dword [ebp + .size]
	call safe_alloc ; pointer to resulting allocation in eax
	popn 1
	mov dword [ebp + .allocation], eax

	; Zero out the memory
	push dword [ebp + .size]
	push 0
	push dword [ebp + .allocation]
	call memset
	popn 3

	mov ebx, dword [ebp + .slice_p] ; pointer to the out slice

	mov eax, dword [ebp + .allocation]
	mov dword [ebx + slice.ptr], eax ; pointer to allocation from malloc

	mov eax, dword [ebp + .size] 
	mov dword [ebx + slice.len], eax ; size of allocation

	mov eax, 0
	leave
	ret
	%pop

slice_from_ptrs:
	%push
	marg .dest:dword, .start:dword, .end:dword
	menter

	mov eax, dword [ebp + .start]
	cmp eax, dword [ebp + .end]
	assert be, "Attempted to create backward slice"

	mov edi, dword [ebp + .dest]
	mov dword [edi + slice.ptr], eax

	mov eax, dword [ebp + .end]
	sub eax, dword [ebp + .start]
	mov dword [edi + slice.len], eax

	mov eax, 0
	leave
	ret
	%pop

slice_from_cstr:
	%push
	marg .dest:dword, .cstr:dword
	menter

	push dword [ebp + .cstr]
	call strlen
	popn 1

	mov ebx, dword [ebp + .dest]
	mov dword [ebx + slice.len], eax
	mov eax, dword [ebp + .cstr]
	mov dword [ebx + slice.ptr], eax

	mov eax, 0
	leave
	ret
	%pop

; returns zero if in bound and 1 if out of bounds
slice_check_bounds:
	%push
	marg .slice_p:dword, .ptr:dword
	menter

	mov eax, dword [ebp + .slice_p]
	mov ebx, dword [eax + slice.ptr]
	cmp ebx, dword [ebp + .ptr]
	jnbe .fail

	add ebx, dword [eax + slice.len]
	cmp dword [ebp + .ptr], ebx 
	jnb .fail

	mov eax, 0
	leave
	ret

	.fail:
	mov eax, 1
	leave
	ret
	%pop

; arguments
; - slice_p: pointer to slice to be `free`d
slice_delete:
	%push
	marg .slice_p:dword
	menter

	mov eax, dword [ebp + .slice_p]
	push dword [eax + slice.ptr]
	call free
	popn 1

	leave
	ret
	%pop

slice_to_owned_cstr:
	%push
	marg .slice_p:dword
	mlocal .allocation:dword
	menter
	
	mov eax, dword [ebp + .slice_p]
	
	mov eax, dword [eax + slice.len]
	inc eax ; +1 for null terminator
	push eax
	call safe_alloc
	popn 1

	mov dword [ebp + .allocation], eax

	mov eax, dword [ebp + .slice_p]
	push dword [eax + slice.len]
	push dword [eax + slice.ptr]
	push dword [ebp + .allocation]
	call memcpy
	popn 3

	; set the null terminator
	mov eax, dword [ebp + .slice_p]
	mov eax, dword [eax + slice.len]
	add eax, dword [ebp + .allocation]
	mov byte [eax], 0

	mov eax, dword [ebp + .allocation]
	leave
	ret
	%pop

slice_copy:
	%push
	marg .dest:dword, .src:dword
	menter

	mov eax, dword [ebp + .src]
	movq xmm0, qword [eax]
	mov eax, dword [ebp + .dest]
	movq qword [eax], xmm0

	leave
	ret
	%pop

slice_advance:
	%push
	marg .slice_ptr:dword, .distance:dword
	menter

	mov eax, dword [ebp + .slice_ptr]
	mov ebx, dword [ebp + .distance]
	add dword [eax + slice.ptr], ebx
	sub dword [eax + slice.len], ebx

	leave
	ret
	%pop

slice_get_end:
	%push
	marg .slice_ptr:dword
	menter

	mov ebx, dword [ebp + .slice_ptr]
	mov eax, dword [ebx + slice.ptr]
	add ebx, dword [ebp + slice.len]

	leave
	ret
	%pop

slice_alloc_copy:
	%push
	marg .slice_ptr:dword, .dest:dword
	mlocal .length:dword
	menter

	mov ebx, dword [ebp + .slice_ptr]
	mov ebx, dword [ebx + slice.len]
	mov dword [ebp + .length], ebx
	inc ebx
	push ebx
	push dword [ebp + .dest]
	call slice_new
	popn 2

	push dword [ebp + .length]
	mov ebx, dword [ebp + .slice_ptr]
	push dword [ebx + slice.ptr]
	mov ebx, dword [ebp + .dest]
	push dword [ebx + slice.ptr]
	call memcpy
	popn 3

	mov eax, dword [ebp + .dest]
	mov eax, dword [eax + slice.ptr]
	add eax, dword [ebp + .length]
	inc eax
	mov byte [eax], 0

	leave
	ret
	%pop
%endif