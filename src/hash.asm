%ifndef _HASH_ASM
%define _HASH_ASM

%include "common.asm"
%include "simd.asm"
%include "slice.asm"

; This code is based on the xxHash32 hashing algortithm
; https://github.com/Cyan4973/xxHash/blob/v0.8.0/xxhash.h#L1188

section .text

xxh32_slice:
    %push
    marg .input_slice:dword, .seed:dword
    menter

    mov eax, dword [ebp + .input_slice]

    push dword [ebp + .seed]
    push dword [eax + slice.len]
    push dword [eax + slice.ptr]
    call xxh32

    leave
    ret
    %pop

xxh32_dword:
    %push
    marg .input:dword, .seed:dword
    menter

    push dword [ebp + .seed]
    push 4
    pushlea eax, dword [ebp + .input]
    call xxh32
    popn 3

    leave
    ret
    %pop
    
xxh32:
    .prime_1 equ 0x9E3779B1
    .prime_2 equ 0x85EBCA77
    .prime_3 equ 0xC2B2AE3D
    .prime_4 equ 0x27D4EB2F
    .prime_5 equ 0x165667B1

    %push
    marg .input:dword, .len:dword, .seed:dword
    mlocal .v4:dword, .v3:dword, .v2:dword, .v1:dword
    menter

    mov esi, dword [ebp + .input]

    mov edi, esi
    add edi, dword [ebp + .len]
    ; edi contains bEnd

    cmp dword [ebp + .len], 16
    jb .short
        sub edi, 15 ; limit = bEnd - 15
        ; edi now contains limit

        mov eax, dword [ebp + .seed]
        mov dword [ebp + .v3], eax ; v3 = seed
        add eax, .prime_2
        mov dword [ebp + .v2], eax ; v2 = seed + prime2
        add eax, .prime_1
        mov dword [ebp + .v1], eax ; v1 = seed + prime1 + prime2

        ; v4 = seed - prime1
        mov eax, dword [ebp + .seed]
        sub eax, .prime_1
        mov dword [ebp + .v4], eax

        ;splat xmm0, dword [seed]
        ;paddd xmm0, [xxh32_v_init]

        ;splat xmm3, dword [xxh32_v_p1]
        ;splat xmm4, dword [xxh32_v_p2]

        .loop_input:

            ; movdqu xmm1, [esi]
            ; add esi, 16

            ; pmulld xmm1, xmm4
            ; paddd xmm0, xmm1

            ; movdqa xmm1, xmm0
            ; pslld xmm0, 13
            ; psrld xmm1, 19
            ; por xmm0, xmm1

            ; pmulld xmm0, xmm3	
            
            mov eax, dword [ebp + .v1]
            mov ebx, dword [esi]
            call xxh32_round
            mov dword [ebp + .v1], eax
            add esi, 4

            mov eax, dword [ebp + .v2]
            mov ebx, dword [esi]
            call xxh32_round
            mov dword [ebp + .v2], eax
            add esi, 4

            mov eax, dword [ebp + .v3]
            mov ebx, dword [esi]
            call xxh32_round
            mov dword [ebp + .v3], eax
            add esi, 4

            mov eax, dword [ebp + .v4]
            mov ebx, dword [esi]
            call xxh32_round
            mov dword [ebp + .v4], eax
            add esi, 4
        cmp esi, edi
        jb .loop_input

        ;movdqu [ebp - 16], xmm0

        mov eax, dword [ebp + .v1]
        rol eax, 1
        mov ebx, dword [ebp + .v2]
        rol ebx, 7
        add eax, ebx
        mov ebx, dword [ebp + .v3]
        rol ebx, 12
        add eax, ebx
        mov ebx, dword [ebp + .v4]
        rol ebx, 18
        add eax, ebx
        jmp .finish_h32
    .short:
        mov eax, dword [ebp + .seed]
        add eax, .prime_5
    
    .finish_h32:
    add eax, dword [ebp + .len]

    push dword [ebp + .len] ; len argument
    push esi ; input argumnt
    push eax ; hash accumulator argument
    call xxh32_finalize
    popn 3

    leave
    ret
    %pop


xxh32_finalize:
    %push
    marg .h32:dword, .ptr:dword, .len:dword
    menter

    mov eax, dword [ebp + .h32]
    mov esi, dword [ebp + .ptr]
    mov ecx, dword [ebp + .len]

    and ecx, 15

    jmp .process4_loop_cond
    .process4_loop:
        mov ebx, dword [esi]
        imul ebx, xxh32.prime_3
        add eax, ebx
        add esi, 4
        rol eax, 17
        imul eax, xxh32.prime_4
        sub ecx, 4
    .process4_loop_cond:
    cmp ecx, 4
    jae .process4_loop

    jmp .process1_loop_cond
    .process1_loop:
        xor ebx, ebx
        mov bl, byte [esi]
        inc esi
        imul ebx, xxh32.prime_5
        add eax, ebx
        rol eax, 11
        imul eax, xxh32.prime_1
        dec ecx
    .process1_loop_cond:
    cmp ecx, 0
    ja .process1_loop
    
    ; xxh32_avalanche subroutine inlined
    mov ebx, eax
    shr ebx, 15
    xor eax, ebx
    imul eax, xxh32.prime_2
    mov ebx, eax
    shr ebx, 13
    xor eax, ebx
    imul eax, xxh32.prime_3
    mov ebx, eax
    shr ebx, 16
    xor eax, ebx

    leave
    ret
    %pop

; eax: acc, ebx: input, returns: eax
xxh32_round:
    imul ebx, xxh32.prime_2
    add eax, ebx
    rol eax, 13
    imul eax, xxh32.prime_1
    ret

%endif