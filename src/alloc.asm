%ifndef _ALLOC_ASM
%define _ALLOC_ASM

segment .text

extern malloc
extern realloc

safe_alloc:
    %push
    marg .size:dword
    menter

    push dword [ebp + .size]
    call malloc
    popn 1

    cmp eax, 0
    assert ne, "Failed allocate"

    leave
    ret
    %pop

safe_realloc:
    %push
    marg .ptr:dword, .size:dword
    menter

    push dword [ebp + .size]
    push dword [ebp + .ptr]
    call realloc
    popn 2

    cmp eax, 0
    assert ne, "Failed to reallocate"

    leave
    ret
    %pop


%endif