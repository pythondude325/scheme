%ifndef _EVAL_ASM
%define _EVAL_ASM

segment .text

lookup:
    %push
    marg .id:dword
    menter

    mov eax, dword [ebp + .id]
    cmp eax, 0x20 ; +
    je .add
    cmp eax, 0x21
    je .multiply
    mprintfn "function %03x", eax

    jmp .add

    die "unsupported function"

    .add:
    mov eax, list_add
    jmp .finish

    .multiply:
    mov eax, list_multiply
    jmp .finish

    .finish:
    leave
    ret
    %pop

list_add:
    %push
    marg .args:dword, .result:dword
    mlocal .temp_result:obj_size, .result_int:dword
    menter

    mov dword [ebp + .result_int], 0

    .item:
    mov eax, [ebp + .args]
    cmp dword [eax + obj.tag], object_tag.pair
    jne .end

    push dword [ebp + .args]
    call pair_car
    popn 1

    pushlea ebx, [ebp + .temp_result]
    push eax
    call eval
    popn 2

    mov eax, dword [ebp + .temp_result + obj.extra]
    add dword [ebp + .result_int], eax

    mprintfn "a: %d", dword [ebp + .result_int]

    mov eax, [ebp + .args]
    call pair_cdr
    popn 1
    mov dword [ebp + .args], eax
    jmp .item

    .end:
    mov eax, dword [ebp + .result]
    mov dword [eax + obj.tag], object_tag.integer
    mov ebx, dword [ebp + .result_int]
    mov dword [eax + obj.extra], ebx

    mov eax, 0
    leave
    ret
    %pop

list_multiply:
    %push
    marg .args:dword, .result:dword
    mlocal .temp_result:obj_size, .result_int:dword
    menter

    mov dword [ebp + .result_int], 1

    .item:
    mov eax, [ebp + .args]
    cmp dword [eax + obj.tag], object_tag.pair
    je .end

    push dword [ebp + .args]
    call pair_car
    popn 1

    pushlea ebx, [ebp + .temp_result]
    push eax
    call eval
    popn 2

    mov eax, dword [ebp + .temp_result + obj.extra]
    mov ebx, dword [ebp + .result_int]
    imul ebx, eax
    mov dword [ebp + .result_int], ebx

    mov eax, [ebp + .args]
    call pair_cdr
    popn 1
    mov dword [ebp + .args], eax
    jmp .item

    .end:
    mov eax, dword [ebp + .result]
    mov dword [eax + obj.tag], object_tag.integer
    mov ebx, dword [ebp + .result_int]
    mov dword [eax + obj.extra], ebx

    mov eax, 0
    leave
    ret
    %pop

eval:
    %push
    marg .expr:dword, .result:dword
    mlocal .function:dword, .tmp_obj2:dword
    menter

    mov eax, dword [ebp + .expr]
    movzx ebx, word [eax + obj.tag]
    cmp ebx, object_tag.pair
    je .function_call
    cmp ebx, object_tag.integer
    je .number

    mprintfn "obj of type: %d", ebx
    die "invalid thing"

    .function_call:
    push dword [ebp + .expr]
    call pair_car
    popn 1

    cmp dword [eax + obj.tag], object_tag.symbol
    assert e, "Function call must have symbol as function"

    push dword [eax + obj.extra]
    call lookup
    popn 1
    mov dword [ebp + .function], eax

    push dword [ebp + .expr]
    call pair_cdr
    popn 1

    push dword [ebp + .result]
    push eax
    call dword [ebp + .function]
    popn 2

    jmp .finish

    .number:
    mov ebx, dword [ebp + .result]
    mov dword [ebx + obj.tag], object_tag.integer
    mov ecx, dword [eax + obj.extra]
    mov dword [ebx + obj.extra], ecx

    .finish:
    mov eax, 0
    leave
    ret
    %pop






%endif