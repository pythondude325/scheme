%ifndef _COMMON_ASM
%define _COMMON_ASM

false equ 0
true equ 1

extern stdin
extern stdout
extern stderr

STDIN equ 0
STDOUT equ 1
STDERR equ 2

segment .rodata
    death_format_specifier: db "ERROR: %s",10,0

extern _exit
extern dprintf
extern printf

%macro popn 1
    add esp, 4*%1
%endmacro

%macro die 1
segment .rodata
    %%death_msg: db %1,0
segment .text
    push %%death_msg
    push death_format_specifier
    push STDERR
    call dprintf
    popn 3

    %ifdef DEBUG
        ; Crash in debug mode
        int3
    %else
        ; Exit in release mode
        push 1
        call _exit
    %endif
%endmacro

%macro assert 1-2 "Assertion failed"
    j%+1 %%pass
        die %2
    %%pass:
%endmacro

%macro mprintfn 1-*
segment .rodata
    %%format_string: db %1,10,0
segment .text
    pusha
    %rep (%0 - 1)
    %rotate -1
        push %1
    %endrep
    push %%format_string
    call printf
    popn %0
    popa
%endmacro

; %macro resstruc 1
;     %if %1._align
;         align %1._align
;     %endif
;     %00:
;     resb %{1}_size
; %endmacro

%macro menter 0
    push ebp
    mov ebp, esp
    %ifdef %$localsize
    sub esp, %$localsize
    %endif
%endmacro

%macro m_parsesize 1
    %strcat %%size_str %1
    %assign %%size_multiplier 1

    ; idk whats up with this
    %assign %%i 0
    %strcat %%char ""

    ; %rep 10
    ;     %strlen %%size_length %%size_str
    ;     %substr %%last_char %%size_str %%size_length

    ;     %if %%last_char != ']'
    ;         %exitrep
    ;     %endif

    ;     %assign %%i %%size_length
    ;     %rep %%size_length
    ;         %substr %%char %%size_str %%i
    ;         %if %%char = '['
    ;             %exitrep
    ;         %endif
    ;         %assign %%i (%%i - 1)
    ;     %endrep

    ;     %if %%i < 0
    ;         %error invalid size specifier syntax %%size_str
    ;     %endif

    ;     %substr %%array_size_str %%size_str (%%i + 1),-2
    ;     %deftok %%array_size %%array_size_str
    ;     %assign %%size_multiplier (%%size_multiplier * %%array_size)

    ;     %substr %%size_str %%size_str 1,(%%i - 1)
    ; %endrep

    %deftok %%size %%size_str

    ; normal size
    %ifidni %%size,byte
        %assign %%base_size 1
    %elifidni %%size,word
        %assign %%base_size 2
    %elifidni %%size,dword
        %assign %%base_size 4
    %elifidni %%size,qword
        %assign %%base_size 8
    %else
        %assign %%base_size %%size
    %endif

    %assign %$arg_size (%%base_size * %%size_multiplier)

%endmacro

%macro m_parsearg 1
    %defstr %%arg_str %1
    %strlen %%arg_length %%arg_str

    %assign %%i 1
    %rep %%arg_length
        %substr %%char %%arg_str %%i
        %if %%char = ':'
            %exitrep
        %endif
        %assign %%i (%%i + 1)
    %endrep

    %if %%i >= %%arg_length
        %fatal invalid argument format %%arg_str
    %endif

    %substr %%arg_name_str %%arg_str 1,(%%i - 1)
    %substr %%arg_size_str %%arg_str (%%i + 1),-1

    %deftok %%arg_name %%arg_name_str

    %substr %%arg_name_first_char %%arg_name_str 1
    %if %%arg_name_first_char != '.'
        %warning "argument identifier should probably begin with ."
    %endif

    %xdefine %$arg_name %%arg_name

    m_parsesize %%arg_size_str
%endmacro

%macro marg 0-*
    %push args

    [absolute 0x8]
    %rep %0
        m_parsearg %1
        %$arg_name: resb %$arg_size
    %rotate 1
    %endrep

    %pop args
    __?SECT?__
%endmacro

%macro mlocal 0-*
    %push locals

    %assign %$$localsize 0
    %rep %0
        m_parsearg %1
        %assign %$$localsize (%$$localsize + %$arg_size)
    %rotate 1
    %endrep

    [absolute -%$$localsize]
    %rep %0
        m_parsearg %1
        %$arg_name: resb %$arg_size
    %rotate 1
    %endrep

    %pop locals
    __?SECT?__
%endmacro

%macro debug 1+.nolist
    %defstr %%string %1
    %warning debug: %%string
%endmacro

%macro enum 1-*
    %push
    %assign %$counter 0
    %1:
    %rep (%0 - 1)
    %rotate 1
        %1 equ %$counter
    %assign %$counter (%$counter + 1)
    %endrep
    %pop
%endmacro

%macro mpush 1-*
    %rep %0
    %rotate -1
        push %1
    %endrep
%endmacro

; Push and effective address and load it into the first argument
%macro pushlea 2
    lea %1, %2
    push %1
%endmacro

; Push an effective address and preserve registers
%macro pushlea 1
    push eax
    lea eax, %1
    xchg eax, dword [esp]
%endmacro

%macro tagged_union 1-*
    %define %%name %[%1]
    %assign %%max_size 0
    %assign %%tag_counter 0

    %rep (%0 - 1)
        %if %2_size > %%max_size
            %assign %%max_size %2_size
        %endif

        %[%%name]_tag.%{2} equ %%tag_counter
        %assign %%tag_counter (%%tag_counter + 1)
    %rotate 1
    %endrep

    struc %%name
        resb %%max_size
        .tag: resd 1
    endstruc
%endmacro




%endif