%ifndef _INTERNER_ASM
%define _INTERNER_ASM

%include "common.asm"
%include "vector.asm"
%include "hashtable.asm"

; # Interner Design
; 
; Keys start at 0x10 to avoid the hashtable markers
;
;

segment .bss
    interner_string_allocations: resb vector_size
    interner_symbol_slices: resb vector_size
    interner_string_table: resb hashtable_size

segment .text

interner_init:
    %push
    menter

    push dword 4
    push dword interner_string_allocations
    call vec_new
    popn 2

    push dword slice_size
    push dword interner_symbol_slices
    call vec_new
    popn 2

    push dword 0x10 ; start at 0x10
    push dword interner_symbol_slices
    call vec_extend
    popn 2

    push dword hashtable_key_slice_vtab
    push dword interner_string_table
    call hashtable_new
    popn 2

    call interner_init_keywords

    leave
    ret
    %pop

 segment .rodata
    keywords:
        .quote: db "quote",0
        .lambda: db "lambda",0
        .if: db "if",0
        .set_bang: db "set!",0
        .begin: db "begin",0
        .cond: db "cond",0
        .and: db "and",0
        .or: db "or",0
        .case: db "case",0
        .let: db "let",0
        .let_star: db "let*",0
        .letrec: db "letrec",0
        .do: db "do",0
        .delay: db "delay",0
        .quasiquote: db "quasiquote",0
        .else: db "else",0
        .arrow: db "=>",0
        .define: db "define",0
        .unquote: db "unquote",0
        .unquote_splicing: db "unquote-splicing",0
        .plus: db "+", 0
        .multiply: db "*", 0

        ; .array: dd .quote, .lambda, .if, .set_bang, .begin, .cond, .and, .or, \
        ;     .case, .let, .let_star, .letrec, .do, .delay, .quasiquote, .else, \
        ;     .arrow, .define, .unquote, .unquote_splicing
        .array: dd .plus, .multiply, .multiply
        .array_len equ ($-.array)/4
segment .text

interner_init_keywords:
    %push
    mlocal .i:dword
    menter

    mov dword [ebp + .i], 0
    .loop:
        mov eax, dword [ebp + .i]
        ; mprintfn "keyword: %s", dword [keywords.array + 4*eax]

        push dword [keywords.array + 4*eax]
        call interner_get_or_intern_cstr
        popn 1
    inc dword [ebp + .i]
    cmp dword [ebp + .i], keywords.array_len
    jb .loop

    leave
    ret
    %pop

interner_deinit:
    %push
    mlocal .allocation_index:dword
    menter

    mov dword [ebp + .allocation_index], 0
    jmp .allocation_free_loop_cond
    .allocation_free_loop:
        mov eax, dword [interner_string_allocations + vector.ptr]
        mov ebx, dword [ebp + .allocation_index]
        mov eax, dword [eax + 4*ebx]

        push eax
        call free
        popn 1
    inc dword [ebp + .allocation_index]
    .allocation_free_loop_cond:
    mov eax, dword [interner_string_allocations + vector.len]
    cmp dword [ebp + .allocation_index], eax
    jb .allocation_free_loop


    push dword interner_string_allocations
    call vec_delete
    popn 1

    push dword interner_symbol_slices
    call vec_delete
    popn 1

    push dword interner_string_table
    call hashtable_delete
    popn 1

    leave
    ret
    %pop

extern strlen

; TODO: write an alternative intern function for static strings

interner_get_or_intern_slice:
    %push
    marg .string_slice_ptr:dword
    mlocal .new_key:dword, .string_allocation:dword
    menter


    ; mov eax, dword [ebp + .string_slice_ptr]
    ; mprintfn "%s", dword [eax + slice.ptr]

    push dword [ebp + .string_slice_ptr]
    push dword interner_string_table
    call hashtable_get
    popn 2

    ; mprintfn "%08x", eax

    cmp eax, 0
    je .not_found
        ; we found the string in the interner; return the old key
        mov eax, dword [eax + table_entry.data]
        leave
        ret
    .not_found:

    push dword [ebp + .string_slice_ptr]
    call slice_to_owned_cstr
    popn 1
    mov dword [ebp + .string_allocation], eax

    ; keep track of the string allocation so we can free it later
    push 1
    push dword interner_string_allocations
    call vec_extend
    popn 2
    mov ebx, dword [ebp + .string_allocation]
    mov dword [eax], ebx

    ; get the value of the new key
    mov ebx, dword [interner_symbol_slices + vector.len]
    mov dword [ebp + .new_key], ebx

    ; make space for the slice
    push dword 1
    push dword interner_symbol_slices
    call vec_extend
    popn 2
    
    ; make slice point to arena4
    mov ebx, dword [ebp + .string_allocation]
    mov dword [eax + slice.ptr], ebx
    mov ebx, dword [ebp + .string_slice_ptr]
    mov ebx, dword [ebx + slice.len]
    mov dword [eax + slice.len], ebx

    ; enter the string's key into the string table
    push dword [ebp + .string_slice_ptr]
    push interner_string_table
    call hashtable_insert
    popn 2

    mov ebx, dword [ebp + .new_key]
    mov dword [eax + table_entry.data], ebx

    ; return the new key
    mov eax, ebx
    leave
    ret
    %pop

interner_get_or_intern_cstr:
    %push
    marg .string:dword
    mlocal .string_slice:slice_size
    menter

    push dword [ebp + .string]
    pushlea eax, [ebp + .string_slice]
    call slice_from_cstr
    popn 2

    pushlea eax, [ebp + .string_slice]
    call interner_get_or_intern_slice
    popn 1

    leave
    ret
    %pop

; Resolve an interned string
;
; Arguments
; - key: key of the string to resolve
; Returns: pointer to slice of string, or null for an invalid key
interner_resolve:
    %push
    marg .key:dword
    menter

    mov ebx, dword [ebp + .key]

    cmp ebx, 0x10
    jb .invalid_key

    cmp ebx, dword [interner_symbol_slices + vector.len]
    jae .invalid_key

    mov eax, dword [interner_symbol_slices + vector.ptr]
    lea eax, dword [eax + ebx*slice_size]

    leave
    ret

    .invalid_key:
    mov eax, 0
    leave
    ret
    %pop

interner_resolve_cstr:
    %push
    marg .key:dword
    menter
    
    push dword [ebp + .key]
    call interner_resolve
    popn 1

    cmp eax, 0
    je .invalid_key

    mov eax, dword [eax + slice.ptr]
    leave
    ret

    .invalid_key:
    mov eax, 0
    leave
    ret
    %pop

interner_dump:
    %push
    mlocal .i:dword
    menter

    mov dword [ebp + .i], 0x10
    jmp .key_loop_cond
    .key_loop:
        push dword [ebp + .i]
        call interner_resolve_cstr
        popn 1

        mprintfn "%03x: '%s'", dword [ebp + .i], eax

    inc dword [ebp + .i]
    .key_loop_cond:
    mov eax, dword [ebp + .i]
    cmp eax, dword [interner_symbol_slices + vector.len]
    jb .key_loop

    leave
    ret
    %pop


%endif