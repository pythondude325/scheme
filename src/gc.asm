%ifndef _GC_ASM
%define _GC_ASM

%include "common.asm"
%include "alloc.asm"
%include "hashtable.asm"
%include "objects.asm"

segment .bss
    gc_allocation_table: resb hashtable_size
    gc_visited_objects: resb hashtable_size

segment .text
	extern free

gc_init:
    %push
    menter

    push dword hashtable_key_int_vtab
    push dword gc_allocation_table
    call hashtable_new
    popn 1

    leave
    ret
    %pop

gc_deinit:
    %push
    menter

    call gc_clear
    call gc_collect

    push dword gc_allocation_table
    call hashtable_delete
    popn 1

    leave
    ret
    %pop

gc_allocate:
    %push
    marg .size:dword
    mlocal .allocation:dword
    menter

    push dword [ebp + .size]
    call safe_alloc
    popn 1

    mov dword [ebp + .allocation], eax

    ; zero out the memory
    cld
    mov al, 0
    mov ecx, dword [ebp + .size]
    mov edi, dword [ebp + .allocation]
    rep stosb

    ; put the allocation into the table
    push dword [ebp + .allocation]
    push dword gc_allocation_table
    call hashtable_insert
    popn 2

    ; Set entry to be default unseen
    mov dword [eax + table_entry.data], 0
    
    mov eax, dword [ebp + .allocation]
    leave
    ret
    %pop

gc_clear:
    %push
    menter

    ; TODO: maybe just clear the table instead of making a new one each time
    push dword hashtable_key_int_vtab
    push dword gc_visited_objects
    call hashtable_new
    popn 1

    ; mark all the allocations as unseen
    mov esi, dword [gc_allocation_table + hashtable.buckets]
    mov eax, 0
    .bucket_loop:
        mov dword [esi + eax*table_entry_size + table_entry.data], 0
    inc eax
    cmp eax, dword [gc_allocation_table + hashtable.capacity]
    jb .bucket_loop

    leave
    ret
    %pop

gc_mark:
    %push
    marg .root:dword
    mlocal .obj_stack:dword, \
        .obj_count:dword, .current_object:dword, \
        .allocs_count:dword, .current_alloc:dword
    menter

    ; TODO: could theoretically replace obj_count with just comparing esp to ebp

    ; initialize local variables
    mov dword [ebp + .allocs_count], 0
    mov dword [ebp + .obj_count], 1

    push 4
    pushlea eax, [ebp + .obj_stack]
    call vec_new
    popn 1

    ; add root as our first object to explore
    push dword [ebp + .root]

    .object_explore_loop:
        pop dword [ebp + .current_object]
        dec dword [ebp + .obj_count]

        ; mprintfn "exploring object %08x", dword [current_object]

        ; if current_object is in `gc_visited_objects`, skip it
        push dword [ebp + .current_object]
        push dword gc_visited_objects
        call hashtable_get
        popn 2
        cmp eax, 0
        jne .object_explore_loop_cont

        push dword [ebp + .current_object]
        call obj_is_terminal
        popn 1

        cmp eax, 1
        je .no_refs
            ; mprintfn "object is non-terminal"

            push dword [ebp + .current_object]
            push dword gc_visited_objects
            call hashtable_insert
            popn 2

            push dword [ebp + .current_object]
            call obj_get_refs_count
            popn 1

            ; mprintfn "object has %d refs", eax
            
            add dword [ebp + .obj_count], eax

            shl eax, 2
            sub esp, eax

            mov eax, esp ; save esp for first argument to obj_get_refs
            push dword [ebp + .current_object]
            push eax
            call obj_get_refs
            popn 2
        .no_refs:


        push dword [ebp + .current_object]
        call obj_get_allocs_count
        popn 1
        mov dword [ebp + .allocs_count], eax

        cmp eax, 0
        je .no_allocs

        shl eax, 2
        sub esp, eax

        mov eax, esp
        push dword [ebp + .current_object]
        push eax
        call obj_get_allocs
        popn 2

        jmp .allocs_loop_cond
        .allocs_loop:
            pop dword [ebp + .current_alloc]
            dec dword [ebp + .allocs_count]

            push dword [ebp + .current_alloc]
            push dword gc_allocation_table
            call hashtable_get
            popn 2

            ; Safety check, shoulnd't be necessary
            cmp eax, 0
            assert ne, "Allocation not found in GC table"

            ; mark the allocation as seen
            mov dword [eax + table_entry.data], 1
        .allocs_loop_cond:
        cmp dword [ebp + .allocs_count], 0
        jnz .allocs_loop

        .no_allocs:
    .object_explore_loop_cont:
    cmp dword [ebp + .obj_count], 0
    jnz .object_explore_loop

    ; finished exploring from this root

    leave
    ret
    %pop

gc_collect:
    %push
    mlocal .bucket_counter:dword, .current_alloc:dword
    menter

    mov dword [ebp + .bucket_counter], 0
    .bucket_loop:
        mov esi, dword [gc_allocation_table + hashtable.buckets]
        mov eax, dword [ebp + .bucket_counter]

        mov ebx, dword [esi + eax*table_entry_size + table_entry.key]
        mov dword [ebp + .current_alloc], ebx
        ; skip over empties
        cmp ebx, hashtable.empty_marker
        je .bucket_loop_cont
        ; skip over tombstones
        cmp ebx, hashtable.tombstone_marker
        je .bucket_loop_cont

        ; don't collect seen allocations
        mov ecx, dword [esi + eax*table_entry_size + table_entry.data]
        cmp ecx, 0 ; unseen allocations have zero
        jne .bucket_loop_cont

        pushlea eax, [esi + eax*table_entry_size]
        push dword gc_allocation_table
        call hashtable_remove_entry
        popn 2

        push dword [ebp + .current_alloc]
        call free
        popn 1

    .bucket_loop_cont:
    inc dword [ebp + .bucket_counter]
    mov eax, dword [ebp + .bucket_counter]
    cmp eax, dword [gc_allocation_table + hashtable.capacity]
    jb .bucket_loop

    push dword gc_visited_objects
    call hashtable_delete
    popn 1

    leave
    ret
    %pop



%endif