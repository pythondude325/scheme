%ifndef _OBJECTS_ASM
%define _OBJECTS_ASM

%include "common.asm"
%include "gc.asm"

%push objects

%assign %$object_tag_count 0

%macro register_object 1
    object_tag.%1 equ %$object_tag_count

    get_refs_count_obj%$object_tag_count equ %1_get_refs_count
    get_refs_obj%$object_tag_count equ %1_get_refs
    get_allocs_count_obj%$object_tag_count equ %1_get_allocs_count
    get_allocs_obj%$object_tag_count equ %1_get_allocs
    is_terminal_obj%$object_tag_count equ %1_is_terminal

    %assign %$object_tag_count %$object_tag_count + 1
%endmacro

struc obj
    .tag: resw 1
    .reserved: resw 1
    .extra: resd 1
endstruc

segment .text

get_count_none:
    mov eax, 0
    ret

get_none:
    ret

%macro obj_get_refs_none 1
    %1_get_refs_count equ get_count_none
    %1_get_refs equ get_none
%endmacro

%macro obj_get_allocs_none 1
    %1_get_allocs_count equ get_count_none
    %1_get_allocs equ get_none
%endmacro

; Include all the files to register their objects

%include "objects/null.asm"
%include "objects/symbol.asm"
%include "objects/pair.asm"
%include "objects/integer.asm"
%include "objects/boolean.asm"

; Finalize the object system

obj_kind_count equ %$object_tag_count

segment .rodata
    obj_get_refs_count_vtab:
    %assign %$objtag_i 0
    %rep obj_kind_count
        dd get_refs_count_obj%$objtag_i
        %assign %$objtag_i %$objtag_i + 1
    %endrep

    obj_get_refs_vtab:
    %assign %$objtag_i 0
    %rep obj_kind_count
        dd get_refs_obj%$objtag_i
        %assign %$objtag_i %$objtag_i + 1
    %endrep

    obj_get_allocs_count_vtab:
    %assign %$objtag_i 0
    %rep obj_kind_count
        dd get_allocs_count_obj%$objtag_i
        %assign %$objtag_i %$objtag_i + 1
    %endrep

    obj_get_allocs_vtab:
    %assign %$objtag_i 0
    %rep obj_kind_count
        dd get_allocs_obj%$objtag_i
        %assign %$objtag_i %$objtag_i + 1
    %endrep

    obj_is_terminal_tab:
    %assign %$objtag_i 0
    %rep obj_kind_count
        db is_terminal_obj%$objtag_i
        %assign %$objtag_i %$objtag_i + 1
    %endrep

segment .text

obj_get_refs_count:
    %push
    marg .src:dword
    menter

    mov eax, dword [ebp + .src]
    movzx eax, word [eax + obj.tag]
    mov eax, dword [obj_get_refs_count_vtab + 4*eax]

    leave
    jmp eax
    %pop

obj_get_refs:
    %push
    marg .dest:dword, .src:dword
    menter

    mov eax, dword [ebp + .src]
    movzx eax, word [eax + obj.tag]
    mov eax, dword [obj_get_refs_vtab + 4*eax]
    
    leave
    jmp eax
    %pop

obj_get_allocs_count:
    %push
    marg .src:dword
    menter

    mov eax, dword [ebp + .src]
    movzx eax, word [eax + obj.tag]
    mov eax, dword [obj_get_allocs_count_vtab + 4*eax]

    leave
    jmp eax
    %pop

obj_get_allocs:
    %push
    marg .dest:dword, .src:dword
    menter

    mov eax, dword [ebp + .src]
    movzx eax, word [eax + obj.tag]
    mov eax, dword [obj_get_allocs_vtab + 4*eax]

    leave
    jmp eax
    %pop

obj_is_terminal:
    %push
    marg .src:dword
    menter

    mov eax, dword [ebp + .src]
    movzx eax, word [eax + obj.tag]
    mov eax, dword [obj_is_terminal_tab + eax]

    leave
    ret
    %pop

%pop objects

%endif