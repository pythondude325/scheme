(define cont '())

(define (foo x y)
    (+ 
        (* 3 x x)
        (* 2 y)))